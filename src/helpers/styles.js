export const formStyleProps = {
  borderColor: 'gray.500',
  _placeholder: { color: 'gray.700' },
  _hover: { borderColor: 'red.300' },
  _focus: { borderColor: 'red.500' },
  size: 'lg',
  my: 3
}
