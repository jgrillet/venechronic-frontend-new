import React from 'react'
import { Banner, Login } from '@/components'

import { Flex } from '@chakra-ui/react'

export const LoginPage = () => {
  return (
    <Flex height='100vh' width='100vw'>
      <Flex w={['100vw', '50vw']} align='center' justify='center'>
        <Login />
      </Flex>
      <Banner />
    </Flex>
  )
}
