import React from 'react'

import {
  Flex,
  Box,
  Stack,
  Drawer,
  DrawerContent,
  DrawerHeader,
  DrawerBody,
  DrawerFooter,
  Divider
} from '@chakra-ui/react'

export const LayoutPage = () => {
  return (
    <Flex h='100vw' w='100vw'>
      <Drawer isOpen placement='left'>
        <DrawerContent bgColor='red.500' color='white' fw='bold'>
          <DrawerHeader>Header</DrawerHeader>
          <Divider />
          <DrawerBody>
            <Stack spacing={4} >
              <Box
                py={2}
                _hover={{ bgColor: 'red.400', cursor: 'pointer' }}
              >
                Citas
              </Box>
              <Box
                py={2}
                _hover={{ bgColor: 'red.400', cursor: 'pointer' }}
              >
                Pacientes
              </Box>
            </Stack>
          </DrawerBody>
          <Divider />
          <DrawerFooter>Footer</DrawerFooter>
        </DrawerContent>
      </Drawer>
    </Flex>
  )
}
