import React from 'react'
import { Flex, Input, Button, Image } from '@chakra-ui/react'

import { formStyleProps } from '@/helpers/styles'

const Login = () => {
  return (
    <Flex direction='column' w={['80vw', '30vw']}>
      <Image src='/logom.svg' my={3} />
      <Input
        {...formStyleProps}
        type='email'
        placeholder='Email o Usuario'
      />
      <Input
        {...formStyleProps}
        type='password'
        placeholder='Contraseña'
      />
      <Button mt={3} colorScheme='red' size='lg'>Iniciar Sesión</Button>
    </Flex>
  )
}

export default Login
