import React from 'react'
import { LayoutPage } from '@/pages/LayoutPage'

export default function App () {
  return (
    <LayoutPage />
  )
}
