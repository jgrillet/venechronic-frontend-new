import React from 'react'
import { Box } from '@chakra-ui/react'

const Banner = () => {
  return (
    <Box
      d={['none', 'block']}
      w='60vw'
      h='100vh'
      bgImage='/fondo_login.jpg'
      bgPosition='center'
      bgSize='cover'
    />
  )
}

export default Banner
