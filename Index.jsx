import React from 'react'
import ReactDOM from 'react-dom'
import { ChakraProvider } from '@chakra-ui/react'
import App from '@/App.jsx'

const Main = () => ((
  <ChakraProvider>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </ChakraProvider>
))

ReactDOM.render(
  <Main />,
  document.getElementById('root')
)
